
Module: Visual Editor
Author: Tidio Ltd. <http://www.tidioelements.com/>

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

Introduction
------------
Add Visual Editor Support to your site. You can edit your site content
with our easy-to-use tool.

Requirements
------------
* None


Installation
------------
* Copy the 'visualeditor' module directory in to your Drupal
sites/all/modules directory as usual.

CONFIGURATION
-------------
After installation plugin will comunicate with Tidio servers and save all
authentication data in drupal settings. No extra configuration is needed.

TROUBLESHOOTING
---------------
If you do not see changes on your website please try to remove cache.

MAINTAINERS
-----------
tytus-tytus
