<?php

include dirname(__FILE__) . '/TidioElementsParser.php';

class VisualEditor {

  private $apiHost = 'http://visual-editor.tidioelements.com/';
  private $platform = 'drupal';
  private $domain;

  /**
   * Gets an site url and save it for later
   * @global type $base_url
   */
  public function __construct() {
    global $base_url;
    $this->domain = $base_url;
  }

  /**
   * Runs before first line of content.
   * 
   * @return type
   */
  public function before() {

    $public = variable_get('tidio-key-public');

    if (isset($public)) {

      if ($this->isAdminArea())
        return;
      TidioElementsParser::start($public);
    }
  }

  /**
   * Runs after content was prepered. Flush the content.
   * 
   * @return type
   */
  public function after() {
    $public = variable_get('tidio-key-public');

    if (isset($public)) {
      if ($this->isAdminArea())
        return;
      TidioElementsParser::end();
    }else {
      $public = $this->getPublicKey();
    }
  }

  /**
   * Gets url for visual editor for the menu.
   * 
   * @return string url
   */
  public function getEditorUrl() {

    $public = $this->getPublicKey();
    $url = '';
    if (isset($public)) {
      $url = $this->apiHost . 'editor-visual/' . $public . '?key=' . $this->getPrivateKey() . '&platform=' . $this->platform;
    }
    return $url;
  }

  /**
   * Performs consistent way to check if current site is admin area or not
   * 
   * @global type $user
   * @return boolean
   */
  public function isAdminArea() {
    $uri = $_GET['q'];
    if (strpos($uri, 'admin') !== FALSE) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Checks if user have permission to $string.
   * 
   * @global type $user
   * @staticvar type $drupal_static_fast
   * @param type $string
   * @param type $account
   * @return boolean
   */
  private function user_access($string, $account = NULL) {
    global $user;

    if (!isset($account)) {
      $account = $user;
    }

    // User #1 has all privileges:
    if ($account->uid == 1) {
      return TRUE;
    }

    // To reduce the number of SQL queries, we cache the user's permissions
    // in a static variable.
    // Use the advanced drupal_static() pattern, since this is called very often.
    static $drupal_static_fast;
    if (!isset($drupal_static_fast)) {
      $drupal_static_fast['perm'] = &drupal_static(__FUNCTION__);
    }
    $perm = &$drupal_static_fast['perm'];
    if (!isset($perm[$account->uid])) {
      $role_permissions = user_role_permissions($account->roles);

      $perms = array();
      foreach ($role_permissions as $one_role) {
        $perms += $one_role;
      }
      $perm[$account->uid] = $perms;
    }

    return isset($perm[$account->uid][$string]);
  }

  /**
   * Checks if user have access to visual editor panel.
   * 
   * @global type $user
   * @return boolean
   */
  public function isAdmin() {

    global $user;
    if ($this->user_access("administer nodes", $user)) {
      return TRUE;
    }
    return in_array('administrator', array_values($user->roles));
  }

  /**
   * Return the public key of visual editor.
   * 
   * @return boolean
   */
  public function getPublicKey() {

    $publicKey = variable_get('tidio-key-public');

    if (isset($publicKey))
      return $publicKey;

    $apiData = $this->getContentData($this->apiHost . 'editor-visual/accessProject?' . http_build_query(array(
                'key' => $this->getPrivateKey(),
                'url' => $this->domain,
                'platform' => $this->platform
    )));
    $apiData = json_decode($apiData, true);
    if (!$apiData['status'])
      return false;

    $apiData = $apiData['value'];

    variable_set('tidio-key-public', $apiData['public_key']);

    return $apiData['public_key'];
  }

  /**
   * Return the private key for visual editor.
   * 
   * @return type
   */
  public function getPrivateKey() {

    $privateKey = variable_get('tidio-key-private');

    if (isset($privateKey))
      return $privateKey;

    $privateKey = md5($this->domain . time());
    variable_set('tidio-key-private', $privateKey);

    return $privateKey;
  }

  /**
   * Download data from remote location.
   * 
   * @param string $url
   * @param type $urlGet
   * @return type
   */
  private function getContentData($url, $urlGet = null) {
    if ($urlGet && is_array($urlGet)) {
      $url = $url . '?' . http_build_query($urlGet);
    }
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
  }

}
